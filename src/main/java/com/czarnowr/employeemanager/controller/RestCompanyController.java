package com.czarnowr.employeemanager.controller;

import com.czarnowr.employeemanager.model.dto.*;
import com.czarnowr.employeemanager.service.RestCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/company/", produces = "application/json; charset=utf-8")
public class RestCompanyController {
    @Autowired
    private RestCompanyService restCompanyService;

    @PutMapping(path = "/addDefault", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addApartment(@RequestBody CreateCompanyDto createCompanyDto) {
        restCompanyService.createDefault(createCompanyDto);
    }

    @GetMapping(path = "/list", produces = "application/json; charset=utf-8")
    public List<CompanyDto> getApartmentList() {
        return restCompanyService.getAll();
    }

    @GetMapping(path = "/getById/{companyId}", produces = "application/json; charset=utf-8")
    public CompanyDto getById(@PathVariable(name = "companyId") Long id) {
        return restCompanyService.getById(id);
    }

    @DeleteMapping(path = "/delete/{companyId}", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteById(@PathVariable(name = "companyId") Long id) {
        restCompanyService.removeById(id);
    }

    @PostMapping(path = "/update/{companyId}", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.OK)
    public CompanyUpdateResult update(@PathVariable(name = "companyId") Long id,
                                      @RequestBody CompanyUpdateRequest updateRequest) {
        CompanyDto companyDto = restCompanyService.update(id, updateRequest);

        return CompanyUpdateResult.createFrom(id, updateRequest, companyDto);
    }

    @PostMapping(path = "/addEmployee/{companyId}/{employeeId}", produces = "application/json; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployeeToCompany(@PathVariable(name = "companyId") Long companyId,
                                     @PathVariable(name = "employeeId") Long employeeId) {
        // AddTenantToApartmentDto
        restCompanyService.addEmployee(companyId, employeeId);
    }

    @GetMapping(path = "/getWithEmployees/{companyId}", produces = "application/json; charset=utf-8")
    public CompanyDto getWithEmployees(@PathVariable(name = "companyId") Long companyId) {
        return restCompanyService.getCompanyWithEmployees(companyId);
    }
}
