package com.czarnowr.employeemanager.controller;

import com.czarnowr.employeemanager.mapper.EmployeeMapper;
import com.czarnowr.employeemanager.model.dto.CreateEmployeeDto;
import com.czarnowr.employeemanager.model.dto.EmployeeDto;
import com.czarnowr.employeemanager.model.dto.EmployeeUpdateRequest;
import com.czarnowr.employeemanager.model.dto.EmployeeUpdateResult;
import com.czarnowr.employeemanager.service.RestEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/employee/")
public class RestEmployeeController {
    @Autowired
    private RestEmployeeService restEmployeeService;

    @Autowired
    private EmployeeMapper employeeMapper;

    @PutMapping(path = "/addDefault", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addApartment(@RequestBody CreateEmployeeDto employeeDto) {
        restEmployeeService.createDefault(employeeDto);
    }

    @GetMapping(path = "/list", produces = "application/json; charset=utf-8")
    public List<EmployeeDto> getApartmentList() {
        return restEmployeeService.getAll();
    }

    @GetMapping(path = "/getById/{employeeId}", produces = "application/json; charset=utf-8")
    public EmployeeDto getById(@PathVariable(name = "employeeId") Long id) {
        return restEmployeeService.getById(id);
    }

    @DeleteMapping(path = "/delete/{employeeId}", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteById(@PathVariable(name = "employeeId") Long id) {
        restEmployeeService.removeById(id);
    }

    @PostMapping(path = "/update/{employeeId}/", produces = "application/json; charset=utf-8")
    @ResponseStatus(value = HttpStatus.OK)
    public EmployeeUpdateResult update(@PathVariable(name = "employeeId") Long id,
                                       @RequestBody EmployeeUpdateRequest updateRequest) {
        EmployeeDto employeeDto = restEmployeeService.update(id, updateRequest);

        return EmployeeUpdateResult.createFrom(id, updateRequest, employeeDto);
    }
}
