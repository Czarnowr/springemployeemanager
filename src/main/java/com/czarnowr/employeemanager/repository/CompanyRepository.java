package com.czarnowr.employeemanager.repository;

import com.czarnowr.employeemanager.model.Company;
import com.czarnowr.employeemanager.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
}
