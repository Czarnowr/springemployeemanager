package com.czarnowr.employeemanager.service;

import com.czarnowr.employeemanager.mapper.CompanyMapper;
import com.czarnowr.employeemanager.model.Company;
import com.czarnowr.employeemanager.model.Employee;
import com.czarnowr.employeemanager.model.dto.*;
import com.czarnowr.employeemanager.repository.CompanyRepository;
import com.czarnowr.employeemanager.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.czarnowr.employeemanager.utility.ModelUtility.copyNonNullProperties;
import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class RestCompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyMapper companyMapper;

    public void createDefault(CreateCompanyDto createCompanyDto) {
        Company company = new Company();
        company.setName(createCompanyDto.getName());
        company.setAddress(createCompanyDto.getAddress());
        companyRepository.save(company);
    }

    @Transactional
    public List<CompanyDto> getAll() {
        return companyRepository.findAll()
                .stream().map(company -> companyMapper.companyToCompanyDto(company))
                .collect(Collectors.toList());
    }

    public CompanyDto getById(Long id) {
        Optional<Company> optionalCompany = companyRepository.findById(id);

        if (!optionalCompany.isPresent()) {
            throw new EntityNotFoundException();
        }
        return companyMapper.companyToCompanyDto(optionalCompany.get());
    }

    public void removeById(Long id) {
        companyRepository.deleteById(id);
    }

    public CompanyDto update(Long id, CompanyUpdateRequest companyUpdateRequest) {
        Optional<Company> optionalCompany = companyRepository.findById(id);
        if (!optionalCompany.isPresent()) {
            throw new EntityNotFoundException();
        }
        Company company = optionalCompany.get();
        copyNonNullProperties(companyUpdateRequest, company);

        companyRepository.save(company);
        return companyMapper.companyToCompanyDto(company);
    }

    @Transactional
    public void addEmployee(Long companyId, Long employeeId) {
        Employee employee = employeeRepository.getOne(employeeId);

        Optional<Company> optionalCompany = companyRepository.findById(companyId);
        if (!optionalCompany.isPresent()) {
            throw new EntityNotFoundException("Company does not exist");
        }

        Company company = optionalCompany.get();
        company.getEmployees().add(employee);

        companyRepository.save(company);
    }

    @Transactional
    public CompanyDto getCompanyWithEmployees(Long companyId) {
        Optional<Company> optionalCompany = companyRepository.findById(companyId);
        if (!optionalCompany.isPresent()) {
            throw new EntityNotFoundException("Company does not exist");
        }

        return companyMapper.companyToCompanyDto(optionalCompany.get());
    }
}
