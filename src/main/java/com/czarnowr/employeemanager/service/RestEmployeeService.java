package com.czarnowr.employeemanager.service;

import com.czarnowr.employeemanager.mapper.EmployeeMapper;
import com.czarnowr.employeemanager.model.Employee;
import com.czarnowr.employeemanager.model.dto.CreateEmployeeDto;
import com.czarnowr.employeemanager.model.dto.EmployeeDto;
import com.czarnowr.employeemanager.model.dto.EmployeeUpdateRequest;
import com.czarnowr.employeemanager.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.czarnowr.employeemanager.utility.ModelUtility.copyNonNullProperties;

@Service
public class RestEmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeMapper employeeMapper;

    public void createDefault(CreateEmployeeDto employeeDto) {
        Employee employee = new Employee();
        employee.setName(employeeDto.getName());
        employee.setSurname(employeeDto.getSurname());
        employee.setEmploymentDate(employeeDto.getEmploymentDate());
        employee.setBirthday(employeeDto.getBirthday());
        employee.setPay(employeeDto.getPay());
        employeeRepository.save(employee);
    }

    public List<EmployeeDto> getAll() {
        return employeeRepository.findAll()
                .stream()
                .map(employee -> employeeMapper.employeeToEmployeeDto(employee))
                .collect(Collectors.toList());
    }

    public EmployeeDto getById(Long id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);

        if (!optionalEmployee.isPresent()) {
            throw new EntityNotFoundException();
        }

        return employeeMapper.employeeToEmployeeDto(optionalEmployee.get());
    }

    public void removeById(Long id) {
        employeeRepository.deleteById(id);
    }

    public EmployeeDto update(Long id, EmployeeUpdateRequest employeeUpdateRequest) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (!optionalEmployee.isPresent()) {
            throw new EntityNotFoundException();
        }

        Employee employee = optionalEmployee.get();
        copyNonNullProperties(employeeUpdateRequest, employee);

        employeeRepository.save(employee);
        return employeeMapper.employeeToEmployeeDto(employee);
    }
}
