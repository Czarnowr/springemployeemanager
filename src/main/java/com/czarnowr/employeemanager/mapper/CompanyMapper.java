package com.czarnowr.employeemanager.mapper;

import com.czarnowr.employeemanager.model.Company;
import com.czarnowr.employeemanager.model.dto.CompanyDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface CompanyMapper {

    @Mappings({
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "address", source = "address")
    })
    Company companyDtoToCompany(CompanyDto companyDto);

    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "address", target = "address"),
            @Mapping(source = "employees", target = "employees")
    })
    CompanyDto companyToCompanyDto(Company company);
}
