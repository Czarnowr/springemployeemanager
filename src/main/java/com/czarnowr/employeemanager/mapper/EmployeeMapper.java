package com.czarnowr.employeemanager.mapper;

import com.czarnowr.employeemanager.model.Employee;
import com.czarnowr.employeemanager.model.dto.EmployeeDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    @Mappings({
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "surname", source = "surname"),
            @Mapping(target = "pay", source = "pay"),
            @Mapping(target = "employmentDate", source = "employmentDate"),
            @Mapping(target = "birthday", source = "birthday")
    })
    Employee employeeDtoToEmployee(EmployeeDto employeeDto);

    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "surname", target = "surname"),
            @Mapping(source = "pay", target = "pay"),
            @Mapping(source = "employmentDate", target = "employmentDate"),
            @Mapping(source = "birthday", target = "birthday")
    })
    EmployeeDto employeeToEmployeeDto(Employee employee);

    @IterableMapping(qualifiedByName = {"toDtos"})
    List<Employee> employeeListToEmployeeDtoList(List<Employee> employees);
}
