package com.czarnowr.employeemanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeUpdateResult {
    private Long id;
    private EmployeeUpdateRequest request;
    private EmployeeDto result;

    public static EmployeeUpdateResult createFrom(Long id, EmployeeUpdateRequest updateRequest, EmployeeDto employeeDto) {
        return new EmployeeUpdateResult(id, updateRequest, employeeDto);
    }
}
