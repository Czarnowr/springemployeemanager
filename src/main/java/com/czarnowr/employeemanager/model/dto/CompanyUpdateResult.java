package com.czarnowr.employeemanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyUpdateResult {
    private Long id;
    private CompanyUpdateRequest request;
    private CompanyDto result;

    public static CompanyUpdateResult createFrom(Long id, CompanyUpdateRequest updateRequest, CompanyDto companyDto) {
        return new CompanyUpdateResult(id, updateRequest, companyDto);
    }
}
