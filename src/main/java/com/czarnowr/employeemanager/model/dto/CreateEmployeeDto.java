package com.czarnowr.employeemanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateEmployeeDto {
    private String name;
    private String surname;
    private Double pay;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate employmentDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
}
