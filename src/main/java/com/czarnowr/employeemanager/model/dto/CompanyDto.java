package com.czarnowr.employeemanager.model.dto;

import com.czarnowr.employeemanager.model.Employee;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDto {
    private String name;
    private String address;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<Employee> employees;
}
